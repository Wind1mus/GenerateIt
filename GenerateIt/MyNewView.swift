//
//  MyNewView.swift
//  GenerateIt
//
//  Created by Дима кодит как не в себя on 26.06.2022.
//

import SwiftUI

struct MyNewView: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct MyNewView_Previews: PreviewProvider {
    static var previews: some View {
        MyNewView()
    }
}
