//
//  GenerateItApp.swift
//  GenerateIt
//
//  Created by Дима кодит как не в себя on 26.06.2022.
//

import SwiftUI

@main
struct GenerateItApp: App {
    var body: some Scene {
        WindowGroup {
            MainView()
        }
    }
}
